delimiter $$

CREATE DATABASE `Temperature` /*!40100 DEFAULT CHARACTER SET swe7 */$$

delimiter $$
USE `Temperature`$$

delimiter $$

CREATE TABLE `Values` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Time` datetime NOT NULL,
  `SensorID` int(11) DEFAULT NULL,
  `Temperature` decimal(4,1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=20067 DEFAULT CHARSET=swe7$$

delimiter $$

CREATE DATABASE `Telldus` /*!40100 DEFAULT CHARACTER SET swe7 */$$

delimiter $$
USE `Telldus`$$

delimiter $$

CREATE TABLE `Devices` (
  `Id` int(11) NOT NULL,
  `Name` varchar(45) DEFAULT NULL,
  `Active` bit(1) DEFAULT b'0',
  `DawnControlled` bit(1) DEFAULT b'0',
  `DuskControlled` bit(1) DEFAULT b'0',
  `TemperatureControlled` bit(1) DEFAULT b'0',
  `State` varchar(45) DEFAULT '""',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7$$
delimiter $$

CREATE TABLE `Timer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Device_Id` int(11) DEFAULT NULL,
  `Action` varchar(45) DEFAULT NULL,
  `Hour` int(11) DEFAULT NULL,
  `Minute` int(11) DEFAULT NULL,
  `Monday` bit(1) DEFAULT NULL,
  `Tuesday` bit(1) DEFAULT NULL,
  `Wednesday` bit(1) DEFAULT NULL,
  `Thursday` bit(1) DEFAULT NULL,
  `Friday` bit(1) DEFAULT NULL,
  `Saturday` bit(1) DEFAULT NULL,
  `Sunday` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=swe7$$



delimiter $$

CREATE TABLE `TemperatureControl` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Device_Id` int(11) DEFAULT NULL,
  `State` varchar(45) DEFAULT NULL,
  `HighTemp` int(11) DEFAULT NULL,
  `LowTemp` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=swe7$$
