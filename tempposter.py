#!/usr/bin/python
# -*- coding: utf-8 -*-

# ******************************************
# This is a python program that handle both
# events from daylight sensors and timer data
# stored in a MySQL database
#
# All login information is stored in a config
# file that you need to update.
# *******************************************

import time
import datetime
import MySQLdb
import urllib
import sys
import logging
import smtplib
import ConfigParser

# Create config parser
Config = ConfigParser.ConfigParser()
# Read config file
Config.read('/home/pi/templogger/templogger.config')
    
# Declare global variables
sTid = "2001-01-01 00:00"
nTries = 0

# Build a mailheader string
mailheader = """From: %s
To: %s
MIME-Version: 1.0
Content-type: text/html
Subject: Raspberry

""" % (Config.get('Mail','Sender'),Config.get('Mail','Receiver'))

# *******************************************
# MailMessage
#
# Function that sends a mail warning for
# lacking temperature readings
# *******************************************
def MailMessage(msg):
    sender = Config.get('Mail','Sender')
    receiver = Config.get('Mail','Receiver')
    message = mailheader + msg
    
    try:
       smtpObj = smtplib.SMTP(Config.get('Mail','SMTP_Server'),Config.get('Mail','SMTP_Port'))
       smtpObj.login(Config.get('Mail','SMTP_User'),Config.get('Mail','SMTP_Password'))
       smtpObj.sendmail(sender, receiver, message)         
       logging.info('Successfully sent email')
    except SMTPException:
        logging.error(sys.exc_info()[0])
       
# *******************************************
# PostTempToNu
#
# Function that is called once a minute
# and checkes for new temperature readnings
# and post new values to temperature.nu
# *******************************************
def PostTempToNu():
    try:
        global sTid
        global nTries

        # Get the latest registered temperature value
        db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Temperature")
        cursor = db.cursor()
        cursor.execute("SELECT Time, Temperature FROM Temperature.Values WHERE SensorID=%s ORDER BY Time DESC LIMIT 1" % Config.get('Sensors','Outdoor'))
        row = cursor.fetchone()
        
        # Check to see if there has registered any new values since the last post
        # Post if there is a new value registered
        if (str(row[0]) != sTid):
            # Clear counter
            nTries = 0
            # Save the time stamp
            sTid = str(row[0])
            # Post data
            url = Config.get('TemperaturNu','Url') + str(row[1])
            u = urllib.urlopen(url)
            data = u.read()
            logging.info('Posted to webservice, Response: %s' % data)
        else:
            # Increase counter
            nTries += 1
            # If 10 minutes has passed, log an warning in the log file 
            if (nTries % 10 == 0):
                # Log a warning
                logging.warning('No new temp log for 10 minutes or more')
            # Send a mail if 30 minutes has passed since the last temperature readning
            if (nTries == 30):
                MailMessage('No new temp log for 30 minutes or more')
        # Close database    
        db.close()
    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)
            
            
# *******************************************
# Main program
#
# *******************************************
try:
    # Set up logging
    logging.basicConfig(filename='/home/pi/log/tempposter.log',format='%(asctime)s: %(levelname)s - %(message)s', level=logging.DEBUG)
    while(1):
        try:
            #Post to Temperature.nu
            PostTempToNu()
            # Wait 60 seconds
            time.sleep(60)
        except:
            logging.error(sys.exc_info()[0])
            sys.exit(1)
except:
    logging.error(sys.exc_info()[0])
finally:
    # Cleanup
    td.unregisterCallback(cb)
    td.close()



