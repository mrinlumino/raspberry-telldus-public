#!/usr/bin/python
# -*- coding: utf-8 -*-

# ******************************************
# This program catches sensor events and
# logges temperature data in a database
#
# *******************************************
import optparse
import td
import time
import datetime
import MySQLdb
import urllib
import logging
import sys
import ConfigParser

# Create config parser
Config = ConfigParser.ConfigParser()

# Initialize
td.init( defaultMethods = td.TELLSTICK_TURNON | td.TELLSTICK_TURNOFF ) #Application can configure to support different methods


# *******************************************
# mySensorEvent
#
# Function that is called
# *******************************************
def mySensorEvent(protocol, model, id, dataType, value, timestamp, callbackId):
    
    try:
        # Fetch date and time
        now = datetime.datetime.now()
        
        if (dataType == 1):
            # Build string for logging to file
            sOut = "ID: %s, Value: %s" % (id, value)
            logging.info('Logged value: %s' % sOut)
            
            # Log to database
            db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Temperature")
            cursor = db.cursor()
            cursor.execute("INSERT INTO Temperature.Values (Time, SensorID, Temperature) VALUES (\"%s\",%s,%s)" % (now.strftime("%Y-%m-%d %H:%M"),id,value))
            db.commit()
            db.close()
    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)
        

            
# *******************************************
# Main program
#
# *******************************************
try:
    # Set up logging
    logging.basicConfig(filename='/home/pi/log/templogger.log',format='%(asctime)s: %(levelname)s - %(message)s', level=logging.DEBUG)

    # Read config file
    Config.read('/home/pi/templogger/templogger.config')
    
    # Register for events
    cb1 = td.registerSensorEvent(mySensorEvent)
        
    # Wait for event
    while(1):
        time.sleep(1)
except:
    logging.error(sys.exc_info()[0])
finally:
    # Cleanup
    td.unregisterCallback(cb1)
    td.close()

