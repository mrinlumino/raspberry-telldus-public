<?php
$pin = $_POST['pin'];
$action = $_POST['Action'];
$deviceid = $_POST['DeviceID'];
$output = array();

$config = parse_ini_file('/home/pi/templogger/php.ini',1);

if ($pin != $config["Main"]["Pin"]) {
    header('Location: login.php');
}

?>


<!DOCTYPE html>

<html>
<head>
    <title>Telldus control</title>
</head>

<body>
<form name="frmTelldus" action="telldus.php" method="post">
    <table>

<?php

if ($pin == '3446')
    {
        if ($action != '' && $deviceid != '') {
            exec("python /home/pi/templogger/tdtool.py --$action $deviceid", $output);
        }
        
        $con = mysql_connect($config["Database"]["Hostname"],$config["Database"]["User"],$config["Database"]["Password"]);
        if (!$con)
          {
          die('Could not connect: ' . mysql_error());
          }
        
        mysql_select_db("Telldus", $con);
        $result = mysql_query("SELECT Id, Name FROM Telldus.Devices WHERE Active = 1",$con);
        $row = mysql_fetch_array($result);
        while ($row) {
            echo '<tr><td>';
            echo $row[0];
            echo ": ";
            echo $row[1];
            echo '</td><td><INPUT type="button" value="Turn ON" onclick="turnOn('; echo $row[0]; echo ');"></td><td><INPUT type="button" value="Turn OFF" onclick="turnOff('; echo $row[0]; echo ');"></td></tr>';
            $row = mysql_fetch_array($result);
        }
        
        mysql_close($con);
    }
?>
    </table>
<?php
echo $output[0];
?>

<input type="hidden" name="pin" id="pin" value="<?php echo $pin;?>" />
<input type="hidden" name="Action" id="Action" value=""/>
<input type="hidden" name="DeviceID" id="DeviceID" value=""/>

</form>
</body>
<script type="text/javascript">
function turnOn(deviceID)
{
    document.getElementById("Action").value = "on";
    document.getElementById("DeviceID").value = deviceID;
    document.frmTelldus.submit();
}
function turnOff(deviceID)
{
    document.getElementById("Action").value = "off";
    document.getElementById("DeviceID").value = deviceID;
    document.frmTelldus.submit();
}
</script>
</html>

