<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('datetime', 'Datum');
        data.addColumn('number', 'Temperatur');
        data.addRows([
<?php

$config = parse_ini_file('/home/pi/templogger/php.ini',1);


$con = mysql_connect($config["Database"]["Hostname"],$config["Database"]["User"],$config["Database"]["Password"]);
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }

$sensorID = $config["Sensors"]["Outdoor"];

mysql_select_db("Temperature", $con);
$result = mysql_query("SELECT YEAR(Time), MONTH(Time)-1, DAY(Time), HOUR(Time), MINUTE(Time), Temperature  FROM Temperature.Values WHERE SensorID=$sensorID AND Time >= DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY ID",$con);
$row = mysql_fetch_array($result);
while ($row) {
    echo "[new Date(";
    echo $row[0]; echo ","; //Year
    echo $row[1]; echo ","; //Month
    echo $row[2]; echo ","; //Day of month
    echo $row[3]; echo ","; //Hour
    echo $row[4]; echo "),"; //Minute
    echo $row[5];
    echo "]";
    if ($row = mysql_fetch_array($result))
    {
        echo ", ";
    }

}

mysql_close($con);

?>                  
        ]);
        var formatter_short = new google.visualization.DateFormat({pattern: 'yy-MM-dd HH:mm'});
        formatter_short.format(data, 0);
        var options = {
          title: 'Outside temperature in Jursla for the last 24 hours',
          hAxis: {format: 'HH:mm'},
          legend: {position: 'none'},
          chartArea: {left: 30, top: 30, height: '85%', width: '85%'}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
    <div id="tempreading" style="width: 900px; height: 100px;">
      <svg width="900" height="500" style="overflow: hidden;">
      <g><text text-anchor="start" x="30" y="19.9" font-family="Arial" font-size="14" font-weight="bold" stroke="none" stroke-width="0" fill="#000000">
      
<?php

$con = mysql_connect($config["Database"]["Hostname"],$config["Database"]["User"],$config["Database"]["Password"]);
if (!$con)
  {
  die('Could not connect: ' . mysql_error());
  }

mysql_select_db("Temperature", $con);
$result = mysql_query("SELECT Time, Temperature  FROM Temperature.Values WHERE SensorID=$sensorID ORDER BY ID DESC LIMIT 1",$con);
$row = mysql_fetch_row($result);
echo "Current temperature: ";
echo $row[1];
echo " &nbsp;&nbsp;Recorded at: ";
echo $row[0];
echo " (UTC)";
mysql_close($con);

?>
      </text></g></svg>
    </div>
  </body>
</html>

