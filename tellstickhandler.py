#!/usr/bin/python
# -*- coding: utf-8 -*-

# ******************************************
# This is a python program that handle both
# events from daylight sensors and timer data
# stored in a MySQL database
#
# All login information is stored in a config
# file that you need to update.
# *******************************************

# Import all necessary libraries
import optparse
import td
import time
import datetime
import MySQLdb
import urllib
import logging
import sys
import ConfigParser

# Declare a variable that will keep day/night state for the program
sTimeState = "Undefined"

# Create config parser
Config = ConfigParser.ConfigParser()

# *******************************************
# WeekDay
#
# Convert weekday index to actual weekday
#
#
# *******************************************
def WeekDay(x):
    return {
        0: 'Monday',
        1: 'Tuesday',
        2: 'Wednesday',
        3: 'Thursday',
        4: 'Friday',
        5: 'Saturday',
        6: 'Sunday'
    }[x]

# *******************************************
# TurnOnDevice
#
# Turns on a specific device bases on ID.
# The function also uses the devicename for
# logging purposes, this may be blank
# *******************************************
def TurnOnDevice(deviceID, deviceName):
    try:
        resCode = td.turnOn(deviceID)
        if resCode != 0:
            res = td.getErrorString(resCode)
            logging.error('Failed to turn on device: %s. Error: %s' % (deviceName, res))
        else:
            logging.info('Successfully turned on device: %s' % deviceName)
            # Log to database
            db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Temperature")
            cursor = db.cursor()
            cursor.execute("UPDATE Telldus.Devices SET State='ON' WHERE Id=%s" % deviceID)
            db.commit()
            db.close()

    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)

# *******************************************
# TurnOffDevice
#
# Turns off a specific device bases on ID.
# The function also uses the devicename for
# logging purposes, this may be blank
# *******************************************
def TurnOffDevice(deviceID, deviceName):
    try:
        resCode = td.turnOff(deviceID)
        if resCode != 0:
            res = td.getErrorString(resCode)
            logging.error('Failed to turn off device: %s. Error: %s' % (deviceName, res))
        else:
            logging.info('Successfully turned off device: %s' % deviceName)
            # Log to database
            db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Temperature")
            cursor = db.cursor()
            cursor.execute("UPDATE Telldus.Devices SET State='OFF' WHERE Id=%s" % deviceID)
            db.commit()
            db.close()

    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)


# *******************************************
# myRawDeviceEvent
#
# Receives and handles events from daylight
# sensor
#
# *******************************************
def myRawDeviceEvent(data, controllerId, callbackId):
    global sTimeState
    try:
        #Check for my light sensor
        if (data.find('protocol:arctech;model:selflearning;house:2483970;unit:10') > -1 ):
            if (data.find('method:turnoff') > -1):
                if sTimeState != 'Daytime':
                    sTimeState = 'Daytime'
                    logging.info('Dawn is here, turning off lights')
                    # Wait ten seconds to let the tellstick devices be idle
                    time.sleep(10)
                    # Fetch all active dawncontrolled devices
                    db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Telldus")
                    cursor = db.cursor()
                    cursor.execute('SELECT Id, Name FROM Telldus.Devices WHERE Active = 1 AND DawnControlled = 1') 
                    results = cursor.fetchall()
                    for row in results:
                        TurnOffDevice(row[0],row[1])
                        # Wait one second
                        time.sleep(1)
                    db.close()
            if (data.find('method:turnon') > -1 ):
                if sTimeState != 'NightTime':
                    sTimeState = 'NightTime'
                    logging.info('Dusk is here, turning on lights')
                    # Wait ten seconds to let the tellstick devices be idle
                    time.sleep(10)
                    # Fetch all active dawncontrolled devices
                    db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Telldus")
                    cursor = db.cursor()
                    cursor.execute('SELECT Id, Name FROM Telldus.Devices WHERE Active = 1 AND DuskControlled = 1') 
                    results = cursor.fetchall()
                    for row in results:
                        TurnOnDevice(row[0],row[1])
                        # Wait one second
                        time.sleep(1)
                    db.close()
    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)



# *******************************************
# doTemperatureControl
#
# Function that is called about ance every
# ten minutes and checkes the latest temp-
# erature reading and switches temperature
# controlled switches off and on at
# configured temperatures
#
# *******************************************
def doTemperatureControl():
    try:
        # Get the latest temperature reading
        db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Temperature")
        cursor = db.cursor()
        cursor.execute("SELECT Temperature FROM Temperature.Values WHERE SensorID=%s ORDER BY ID DESC LIMIT 1" % Config.get('Sensors','Outdoor'))
        row = cursor.fetchone()
        nCurrentTemp = row[0]
        
        # Get the actions for the current temperature
        cursor.execute("SELECT d.Id, d.Name, tc.State FROM Telldus.Devices d INNER JOIN Telldus.TemperatureControl tc WHERE d.Active = 1 AND d.TemperatureControlled = 1 AND %.0f<=tc.HighTemp AND %.0f>=tc.LowTemp" % (nCurrentTemp,nCurrentTemp))
        results = cursor.fetchall()
        for row in results:

            #Get the current state
            cur2 = db.cursor()
            cur2.execute("SELECT State FROM Telldus.Devices WHERE Id=%s" % row[0])
            stateRow = cur2.fetchone()
            sState = stateRow[0]
            # Check action
            if row[2] == 'ON' and sState != "ON":
                logging.info("TemperatureControl. Current temp: %s. Unit info: %s-%s-%s. Current state: %s" % (nCurrentTemp,row[0],row[1],row[2], sState))
                # Turn on device
                TurnOnDevice(row[0], row[1])
            elif row[2] == 'OFF' and sState != "OFF":
                logging.info("TemperatureControl. Current temp: %s. Unit info: %s-%s-%s. Current state: %s" % (nCurrentTemp,row[0],row[1],row[2], sState))
                # Turn off device:
                TurnOffDevice(row[0],row[1])
            # Wait one second
            time.sleep(1)
    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)        




# *******************************************
# HandleTimeEvent
#
# Function that is called at least once a
# minute and checkes the database for stored
# timer events and calles on/off functions if
# needed
# *******************************************
def HandleTimeEvent():
    try:
        # Get time of day
        now = datetime.datetime.now()
        
        # Get the activities for the curret day, hour and minute
        db = MySQLdb.connect(Config.get('Database','Hostname'),Config.get('Database','User'),Config.get('Database','Password'),"Telldus")
        cursor = db.cursor()
        cursor.execute('SELECT t.Device_Id, d.Name, t.Action FROM Telldus.Timer t INNER JOIN Telldus.Devices d ON d.Id = t.Device_ID WHERE d.Active = 1 AND t.Hour=%s AND t.Minute=%s AND t.%s=1' % (now.hour, now.minute, WeekDay(datetime.datetime.today().weekday()))) 
        results = cursor.fetchall()
        for row in results:
            # Check action
            if row[2] == 'ON':
                # Turn on device
                TurnOnDevice(row[0], row[1])
            elif row[2] == 'OFF':
                # Turn off device:
                TurnOffDevice(row[0],row[1])
            # Wait one second
            time.sleep(1)
    except:
        logging.error(sys.exc_info()[0])
        sys.exit(1)
    finally:
        db.close()
            
# *******************************************
# Main program
#
# Sets up event handlers and the does an
# eternal loop calling HandleTimeEvent every
# minute
# *******************************************
try:
    # Set up logging
    logging.basicConfig(filename='/home/pi/log/tellstickhandler.log',format='%(asctime)s: %(levelname)s - %(message)s', level=logging.DEBUG)

    # Read config file
    Config.read('/home/pi/templogger/templogger.config')

    # Initialize the telldus functions
    td.init( defaultMethods = td.TELLSTICK_TURNON | td.TELLSTICK_TURNOFF ) #Application can configure to support different methods

    # Register for events
    cb = td.registerRawDeviceEvent(myRawDeviceEvent)

    # Call HandleTimeEvent every minute
    nMinuteCounter = 0
    while(1):
        HandleTimeEvent()
        time.sleep(58)
        nMinuteCounter += 1
        if nMinuteCounter == 10:
            doTemperatureControl()
            nMinuteCounter = 0
except:
    logging.error(sys.exc_info()[0])
finally:
    # Cleanup
    td.unregisterCallback(cb)
    td.close()

